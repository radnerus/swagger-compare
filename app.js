const fs =require('fs');

let developJSON;
let codeCleanUpJSON;

/**
 * Create a file called 'develop.json' using the swagger of develop API
 * Create a file called 'code_clean_up.json' using the swagger of code clean up API
 * run using 'node app.js'
 * removed api will be listed in 
 */

const loadJSONData = async () => {
    developJSON = JSON.parse(fs.readFileSync('develop.json'));
    codeCleanUpJSON = JSON.parse(fs.readFileSync('code_clean_up.json'));

    const developAPI = Object.keys(developJSON.paths);
    const codeCleanUpAPI = Object.keys(codeCleanUpJSON.paths);

    const unmatched = developAPI.filter((value) => !codeCleanUpAPI.includes(value));
    fs.writeFileSync('removed.json', JSON.stringify(unmatched));

    console.log(developAPI.length, codeCleanUpAPI.length, unmatched.length);
}

loadJSONData();

